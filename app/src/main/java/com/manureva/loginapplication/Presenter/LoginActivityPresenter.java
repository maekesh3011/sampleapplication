package com.manureva.loginapplication.Presenter;

import android.support.annotation.Nullable;

import com.manureva.loginapplication.Interface.LoginActivityMVP;
import com.manureva.loginapplication.Network.LoginResponse;

/**
 * Created by maekesh on 07/07/17.
 */

public class LoginActivityPresenter implements LoginActivityMVP.Presenter {

    @Nullable
    LoginActivityMVP.View view;
    LoginActivityMVP.Model model;

    public LoginActivityPresenter(LoginActivityMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(LoginActivityMVP.View view) {
        this.view = view;
    }

    @Override
    public void loginBtnClicked() {

        if (view != null) {

            if (view.getEmail().trim().equals("") || view.getPassword().trim().equals("")) {
                view.showInputError();
            } else  {
                model.loginReq(view.getEmail(), view.getPassword());

                // suggesstion or logic idea
                LoginResponse loginResponse = model.loginRes();

                if (loginResponse.equals("Success")) {
                    view.showSuccessMessage();
                } else {
                    view.showErrorMessage();
                }

            }
        }

    }
}
