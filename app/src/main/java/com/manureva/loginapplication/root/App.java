package com.manureva.loginapplication.root;

import android.app.Application;

/**
 * Created by maekesh on 06/07/17.
 */

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }


    public ApplicationComponent getApplicationComponent(){
        return component;
    }
}
