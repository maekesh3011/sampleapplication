package com.manureva.loginapplication.root;

import com.manureva.loginapplication.Activity.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by maekesh on 06/07/17.
 */


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(LoginActivity target);
}
