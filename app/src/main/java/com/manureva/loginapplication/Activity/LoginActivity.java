package com.manureva.loginapplication.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.manureva.loginapplication.CustomWidget.CustomEditText;
import com.manureva.loginapplication.CustomWidget.CustomTextInputLayout;
import com.manureva.loginapplication.CustomWidget.CustomTextView;
import com.manureva.loginapplication.Interface.LoginActivityMVP;
import com.manureva.loginapplication.R;
import com.manureva.loginapplication.root.App;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginActivityMVP.View {

    @Inject
    LoginActivityMVP.Presenter presenter;

    @BindView(R.id.linearHeader)
    LinearLayout linearHeader;

    @BindView(R.id.LoginTitle)
    CustomTextView LoginTitle;

    @BindView(R.id.input_email)
    CustomEditText inputEmail;

    @BindView(R.id.input_layout_email)
    CustomTextInputLayout inputLayoutEmail;

    @BindView(R.id.input_password)
    CustomEditText inputPassword;

    @BindView(R.id.input_layout_password)
    CustomTextInputLayout inputLayoutPassword;

    @BindView(R.id.logintxt)
    CustomTextView logintxt;

    @BindView(R.id.forgottxt)
    CustomTextView forgottxt;

    @BindView(R.id.ortxt)
    CustomTextView ortxt;

    @BindView(R.id.signuptxt)
    CustomTextView signuptxt;

    @BindView(R.id.forgotandsignuplayout)
    LinearLayout forgotandsignuplayout;

    @BindView(R.id.googleplusImg)
    ImageView googleplusImg;

    @BindView(R.id.googleplusFrame)
    FrameLayout googleplusFrame;

    @BindView(R.id.facebookImg)
    ImageView facebookImg;

    @BindView(R.id.facebookFrame)
    FrameLayout facebookFrame;

    @BindView(R.id.histogramImg)
    ImageView histogramImg;

    @BindView(R.id.histogramFrame)
    FrameLayout histogramFrame;

    @BindView(R.id.socialLayout)
    LinearLayout socialLayout;

    @BindView(R.id.linearBody)
    LinearLayout linearBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((App) getApplication()).getApplicationComponent().inject(this);

        logintxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loginBtnClicked();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.setView(this);
    }

    @Override
    public String getEmail() {
        return inputEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return inputPassword.getText().toString();
    }

    @Override
    public void showErrorMessage() {

            // Time being use Toast by change to Alert
        Toast.makeText(this, "Login Failed! Please Try again or Register", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInputError() {

        inputEmail.setError("Email and password shln't be Empty");
        inputPassword.setError("Email and password shln't be Empty");

    }

    @Override
    public void showSuccessMessage() {

        Toast.makeText(this, "Login Succeed", Toast.LENGTH_SHORT).show();

    }
}
