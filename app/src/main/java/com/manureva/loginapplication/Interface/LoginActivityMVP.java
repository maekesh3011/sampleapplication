package com.manureva.loginapplication.Interface;

import com.manureva.loginapplication.Network.LoginResponse;

/**
 * Created by maekesh on 07/07/17.
 */

public interface LoginActivityMVP {

    interface View {

        //input details
        String getEmail();
        String getPassword();


        //output Details
        void showErrorMessage();
        void showInputError();
        void showSuccessMessage();

    }

    interface Presenter {
        //set view in the presenter

        void setView(LoginActivityMVP.View view);

        //action happened click event
        void loginBtnClicked();

    }

    interface Model {

        //send and receive data to the backend

        void loginReq(String email, String password);
        LoginResponse loginRes();

    }
}
